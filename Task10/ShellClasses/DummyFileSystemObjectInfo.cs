﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace TreeSize.ShellClasses
{
    internal class DummyFileSystemObjectInfo : FileSystemObjectInfo
    {
        public DummyFileSystemObjectInfo()
            : base(new DirectoryInfo("DummyFileSystemObjectInfo"))
        {
        }
    }

}
