﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Data;

namespace TreeSize.Converters
{
    public class FileSizeFormatConverter : IValueConverter
    {
        private const string _dash = "-";
        
        [DllImport("Shlwapi.dll", CharSet = CharSet.Auto)]
        private static extern int StrFormatByteSize(
            long fileSize,
            [MarshalAs(UnmanagedType.LPTStr)] StringBuilder buffer,
            int bufferSize);

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is long size)
            {
                if (size == long.MaxValue)
                    return _dash;

                StringBuilder sb = new StringBuilder(20);
                StrFormatByteSize(size, sb, 20);
                return sb.ToString();
            }
            return _dash;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

}
