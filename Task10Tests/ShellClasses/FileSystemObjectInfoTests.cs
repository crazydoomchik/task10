﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;

namespace TreeSize.ShellClasses.Tests
{

    public class FileSystemObjectInfoTests
    {
        [Test()]
        public void DirSizeTest()
        {
            var pathToDirectory = AppDomain.CurrentDomain.BaseDirectory + "//TestFolder";

            var files = new List<string>()
            {
                { pathToDirectory + "//Test.txt" },
                { pathToDirectory + "//Test1.txt" },
                { pathToDirectory + "//Test2.txt" }
            };      

            var directory = new DirectoryInfo(pathToDirectory);
            var size = FileSystemObjectInfo.DirSize(directory, 1, 0, 20);
            double resultSize = 0;

            foreach(var item in files)
            {
                resultSize += new System.IO.FileInfo(item).Length;
            }

            Assert.AreEqual(resultSize, size);
        }

        [Test]
        public void DontExistDirectory()
        {
            try
            {
                var pathToDirectory = @"C:\DoesNotExistFolder";
                var directory = new DirectoryInfo(pathToDirectory);
                var size = FileSystemObjectInfo.DirSize(directory, 1, 0, 20);
                Assert.AreEqual(1, size);
            }
            catch(DirectoryNotFoundException exe)
            {
                StringAssert.Contains(exe.Message, "Directory does not exist");
            }
            
        }
        
    }
}